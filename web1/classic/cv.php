

<?php
include('../../getsimple/theme/ElegantOne/header.inc.php');  ?>


<body id="top">
<div id="cv" class="instaFade">
	<div class="mainDetails">
		<div id="headshot" class="quickFade">
			<img src="Trond_Havard_Hanssen2.jpg" alt="Alan Smith" />
		</div>
		
		<div id="name">
			<h1 class="quickFade delayTwo">Trond Håvard Hanssen</h1>
			<h2 class="quickFade delayThree">Drift og Utviklingsleder</h2>
		</div>
		
		<div id="contactDetails" class="quickFade delayFour">
			<ul>
				<li><img src="icons/a.png"> <a href="mailto:trond@hanssen.me" target="_blank">trond@hanssen.me</a></li>
				<li><img src="icons/firefox.png" width="20px"> <a href="http://trond.hanssen.me">http://trond.hanssen.me</a></li>
				<li><img src="icons/phone.png" width="20px"> +47 916 99 116</li>
			</ul>
			
		</div>
		<div id="social" class="quickFade delayFour">
			<ul>
				<li><img src="icons/linkedin.png" width="25px"> <A href="http://no.linkedin.com/in/trondh">no.linkedin.com/in/trondh/</a></li>
				<li><img src="icons/fb.png" width="20px"> <A href="Facebook.com/">facebook.com/trond.hanssen</a></li>
				<li><img src="icons/twitter.png" width="20px"> <A href="http://twitter.com/trond7">twitter.com/trond7</a></li>

			</ul>
		</div>	
		<div class="clear"></div>
		
	</div>
	
	<div id="mainArea" class="quickFade delayFive">
	
		<section>
			<article>
				<div class="sectionTitle">
					<h1>Om meg</h1>
				</div>
				
				<div class="sectionContent">
					<p>Jeg er 39 år og tobarnsfar. De siste 10 årene har jeg arbeidet i samme organisasjon og har gradvis fått tildelt økt ansvar. 
					Det økte ansvaret har jeg først og fremst fått for følgende utpregende egenskaper: Godt overblikk, gode pedagosiske egenskaper, gode sosiale egenskaper </p>
				</div>
			</article>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitle">
				<h1>Arbeidserfaring</h1>
			</div>
			
			<div class="sectionContent">
				<article>
					<h2>Drift og utviklingsleder, <a href="http://iktsenteret.no">Senter for IKT i utdanningen</a></h2>
					<p class="subDetails">Februar 2010 - Nå</p>
					<p>Teknisk avnsvarlig for drift og utviklings av iktsenteret sin portefølje av nettsider. I overkant av 20 produksjonssatte nettsider.</p>
				</article>
				
				<article>
					<h2>Utviklingsleder, <a href="http://utdanning.no/">Utdanning.no</a></h2>
					<p class="subDetails">Mars 2008 - Februar 2010</p>
					<p>Teknisk ansvarlig for videreutvikling av portalen Utdanning.no. Avdelingsleder med personalansvar for 6 utviklere</p>
				</article>
				<article>
					<h2>Utvikler / prosjektleder, <a href="http://utdanning.no/">Utdanning.no</a></h2>
					<p class="subDetails">Mars 2005 - Februar 2008</p>
					<p>Prosjektleder for en webbasert portal med digitale læremidler</p>
				</article>
				<article>
					<h2>Daglig leder, Norskwebconsult.no</h2>
					<p class="subDetails">Januar 2000 - Januar 2005</p>
					<p>Selvstendig næringsdrivende. Utført ulike webutviklingsoppdrag under studietiden</p>
				</article>
			</div>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitle">
				<h1>Kompetanse</h1>
			</div>
			
			<div class="sectionContent">
				<ul class="keySkills">
					<li>Webutvikling</li>
					<li>Universiell utforming</li>
					<li>Tjenestebasert arkitektur</li>
					<li>HTML5</li>
					<li>CSS3</li>
					<li>PHP</li>
					<li>MYSQL</li>
					<li>LAMP</li>
					<li>Drupal CMS</li>
					
				</ul>
			</div>
			<div class="clear"></div>
		</section>
		
		
		<section>
			<div class="sectionTitle">
				<h1>Utdannelse</h1>
			</div>
			
			<div class="sectionContent">
				<article>
					<h2>NTNU</h2>
					<p class="subDetails">Master/Hovedfag informatikk</p>
					<p></p>
				</article>
				
				<article>
					<h2>Ringe videregående</h2>
					<p class="subDetails">Studiespesialisering</p>
					<p>Fordypning i data og media</p>
				</article>
			</div>
			<div class="clear"></div>
		</section>
		
	</div>
</div>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-3753241-1");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>